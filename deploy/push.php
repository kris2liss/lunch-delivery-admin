<?php
$deployDir = __DIR__ . DIRECTORY_SEPARATOR;

$sourceFolder = __DIR__ . DIRECTORY_SEPARATOR . '..';

$gitStatus = command_exec('git status --porcelain', $sourceFolder);

if (strlen($gitStatus) > 0) {
  exit("\e[31m\e[7mWorking folder is not clean!\e[27m\e[0m\n");
}

$commitId = command_exec('git show -s --format=%h', $sourceFolder);
$commitId = preg_replace('/\s*/', '', $commitId);

$buildFolder = $deployDir . 'public';

$currentConfig = $sourceFolder . DIRECTORY_SEPARATOR . 'ApiConfig.js';
$backupConfig = $sourceFolder . DIRECTORY_SEPARATOR . 'ApiConfig.js.bak';
$prodConfig = $deployDir . 'ApiConfig.js';

renameFile($currentConfig, $backupConfig);
renameFile($prodConfig, $currentConfig);

command_exec('yarn parcel build ' . $sourceFolder . '/src/index.html --no-source-maps --dist-dir ' . $buildFolder, $sourceFolder);

renameFile($currentConfig, $prodConfig);
renameFile($backupConfig, $currentConfig);

copy($deployDir . '.htaccess' , $buildFolder . DIRECTORY_SEPARATOR . '.htaccess');
copy($deployDir . 'robots.txt' , $buildFolder . DIRECTORY_SEPARATOR . 'robots.txt');

$zipFile = sys_get_temp_dir() . "/deploy.zip";

if (file_exists($zipFile)) {
  deleteFile($zipFile);
}

$folders = ['public'];
zip_files(__DIR__, $folders, $zipFile);
rrmdir($buildFolder);

command_exec('scp ' . $zipFile . ' u0114284@scp106.hosting.reg.ru:/var/www/u0114284/tmp/', __DIR__);
unlink($zipFile);

$scriptFile = $deployDir . "deploy.sh";
command_exec('ssh u0114284@scp106.hosting.reg.ru "bash -s" < ' . $scriptFile . ' ' . $commitId, __DIR__);

$logFile = $deployDir . 'deploy.log';
$event = date("Y-m-d H:i:s") . " $commitId\n";
file_put_contents($logFile, $event, FILE_APPEND | LOCK_EX);

function command_exec($command, $cwd)
{
  $descriptorspec = array(
    0 => array("pipe", "r"),  // stdin - канал, из которого дочерний процесс будет читать
    1 => array("pipe", "w"),  // stdout - канал, в который дочерний процесс будет записывать
    2 => array("pipe", "w")   // stderr - файл для записи
  );

  $process = proc_open($command, $descriptorspec, $pipes, $cwd);

  if (is_resource($process)) {
    // $pipes теперь выглядит так:
    // 0 => записывающий обработчик, подключённый к дочернему stdin
    // 1 => читающий обработчик, подключённый к дочернему stdout
    // 2 => читающий обработчик, подключённый к дочернему stderr

    fclose($pipes[0]);

    $output = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $errors = stream_get_contents($pipes[2]);
    fclose($pipes[2]);

    $errors = str_replace("stdin: is not a tty\n", '', $errors);
    if (strlen($errors) > 0) {
      print($errors);
      print("\n");
    }

    // Важно закрывать все каналы перед вызовом
    // proc_close во избежание мёртвой блокировки
    $return_value = proc_close($process);
  }

  return $output;
}

function zip_files($parent, $folders, $destination)
{
  $zip = new ZipArchive();
  if ($zip->open($destination, ZIPARCHIVE::CREATE) === true) {
    $parentFolder = realpath($parent);
    foreach ($folders as $folder) {
      $source = realpath($parentFolder . DIRECTORY_SEPARATOR . $folder);
      if (is_dir($source)) {
        $iterator = new RecursiveDirectoryIterator($source);
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
        foreach ($files as $file) {
          $file = realpath($file);
          $zipPath = str_replace($parentFolder . DIRECTORY_SEPARATOR, '', $file);
          $zipPath = str_replace('\\', '/', $zipPath);
          if (is_dir($file) && (count(glob("$file/*")) === 0)) {
            $zip->addEmptyDir($zipPath);
          } elseif (is_file($file)) {
            $zip->addFile($file, $zipPath);
          }
        }
      }
    }
  } else {
    exit('Error open ZIP-file.');
  }
  return $zip->close();
}

function renameFile($oldName, $newName)
{
  if (!rename($oldName, $newName)) {
    exit('\e[31m\e[7mError when rename "' . $oldName . '" to "' . $newName . '\e[27m\e[0m\n');
  }
}

function deleteFile($name)
{
  if (!unlink($name)) {
    exit('\e[31m\e[7mError when delete "' . $name . '\e[27m\e[0m\n');
  }
}

function rrmdir($src)
{
  $dir = opendir($src);
  while (false !== ($file = readdir($dir))) {
    if (($file != '.') && ($file != '..')) {
      $full = $src . '/' . $file;
      if (is_dir($full)) {
        rrmdir($full);
      } else {
        unlink($full);
      }
    }
  }
  closedir($dir);
  rmdir($src);
}
