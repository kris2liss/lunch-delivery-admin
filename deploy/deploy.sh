#!/bin/sh

DEST_FOLDER=/var/www/u0114284/apps/lunch-admin/
CURRENT_FOLDER=$DEST_FOLDER"current"
DEPLOY_FOLDER=$DEST_FOLDER$1"-$(date +%s)"

mkdir -p $DEPLOY_FOLDER
unzip /var/www/u0114284/tmp/deploy.zip -d $DEPLOY_FOLDER
rm /var/www/u0114284/tmp/deploy.zip

ln -sfn $DEPLOY_FOLDER $CURRENT_FOLDER
