module.exports = ({ m, BulmaCalendarComp, MenuStore, DishFormComp, AddDishPanelComp, DateFieldComp }) => () => {
  let MenuDate = sqlDateFormat(new Date())

  const DuplicateDishesUntilEndWeekButtons = () => ({
    confirm: false,
    success: false,
    loading: false,
    submit (typeId, replace) {
      this.confirm = false
      this.loading = true
      MenuStore.fetchItem(MenuDate)
      .then(items => {
        const selectedDishes = items ? items.filter(item => item.typeId === typeId) : []
        const selectedDate = new Date(MenuDate)
        let dayWeek = selectedDate.getDay()
        let nextDate = selectedDate
        const addingPromises = [];
        const removingPromises = [];
        for (; dayWeek < 5; dayWeek++) {
          nextDate.setDate(nextDate.getDate() + 1)
          let handledDate = sqlDateFormat(nextDate)
          MenuStore.fetchItem(handledDate)
          .then(items => {
            let oldDishes = (items ? items.filter(dish => dish.typeId === typeId) : [])
            let newDishes = selectedDishes
            if(oldDishes.length > 0) {
              newDishes = newDishes.filter(newDish => {
                const sameOldDishes = oldDishes.filter(oldDish => newDish.id === oldDish.id)
                return sameOldDishes.length === 0
              })
              oldDishes = oldDishes.filter(oldDish => {
                return 0 === selectedDishes.filter(selectedDish => selectedDish.id === oldDish.id).length
              })
              if (replace) {
                oldDishes.forEach(dish => removingPromises.push(MenuStore.removeDishFromMenu(dish.id, handledDate)))
              }
            }

            newDishes.forEach(dish => addingPromises.push(MenuStore.addDishToMenu(dish.id, handledDate)))
          })
          Promise.all(addingPromises.concat(removingPromises)).then(() => (this.success = true, this.loading = false))
        }
      })
    },
    view (vnode) {
      return [
        m('button.button.is-small.is-white.is-outlined',
          {
            class: (this.confirm) ? 'is-hidden' : '',
            onclick: event => { this.confirm = true; event.preventDefault(); event.stopPropagation() }
          },
          'До конца недели'
        ),
        m('.buttons.has-addons',
          m('button.button.is-white.is-small',
            {
              class: this.confirm ? '' : 'is-hidden',
              onclick: () => (this.confirm = false)
            },
            'Не повторять'
          ),
          m('button.button.is-link.is-small',
            {
              class: this.confirm ? '' : 'is-hidden',
              onclick: event => { this.submit(vnode.attrs.key, false); event.preventDefault(); event.stopPropagation() }
            },
            'Добавить!'
          ),
          m('button.button.is-warning.is-small',
            {
              class: this.confirm ? '' : 'is-hidden',
              onclick: event => { this.submit(vnode.attrs.key, true); event.preventDefault(); event.stopPropagation() }
            },
            'Заменить!'
          )
        )
      ]
    }
  })

  const RemoveButtons = () => ({
    confirmDelete: false,
    submit (id, callback) {
      MenuStore.removeDishFromMenu(id, MenuDate).then(() => callback())
    },
    view (vnode) {
      return [
        m('button.delete.is-medium',
          {
            class: (this.confirmDelete) ? 'is-hidden' : '',
            onclick: event => { this.confirmDelete = true; event.preventDefault(); event.stopPropagation() }
          },
          'X'
        ),
        m('.buttons.has-addons',
          m('button.button.is-outlined.is-small',
            {
              class: this.confirmDelete ? '' : 'is-hidden',
              onclick: () => (this.confirmDelete = false)
            },
            'Не убирать'
          ),
          m('button.button.is-outlined.is-danger.is-small',
            {
              class: this.confirmDelete ? '' : 'is-hidden',
              onclick: event => { this.submit(vnode.attrs.key, vnode.attrs.onRemoveCallback); event.preventDefault(); event.stopPropagation() }
            },
            'Убрать!'
          )
        )
      ]
    }
  })

  let lastDishTypeId
  const RowMarkup = ({ item, onRemoveCallback }) => {
    let result = m('tr.is-clickable',
      { onclick: () => DishFormComp.showEditForm(item) },
      m('td',
        m('.level.is-mobile.mb-0',
          m('.level-left.is-flex-shrink-1', item.name),
          m('.level-right',
            m(RemoveButtons, { key: item.id, onRemoveCallback })
          )
        ),
        m('.block.is-size-7', item.comment)
      )
    )
    if (lastDishTypeId !== item.typeId) {
      result = [
        m('tr.is-selected',
          m('td',
            m('.level.is-mobile',
              m('.level-left', item.typeName),
              m('.level-right', 
                m(DuplicateDishesUntilEndWeekButtons, { key: item.typeId })
              )
            )
          )
        ),
        result
      ]
      lastDishTypeId = item.typeId
    }

    return result
  }

  const TableMarkup = (items, onRemoveCallback) => {
    const result = m('table.table.is-hoverable.is-fullwidth',
      m('tbody',
        items.sort((dish1, dish2) => dish1.typeOrder - dish2.typeOrder).map(item => RowMarkup({ item, onRemoveCallback }))
      )
    )
    lastDishTypeId = ''
    return result
  }

  const TableComp = {
    view (vnode) {
      return vnode.attrs.items.length > 0 ? TableMarkup(vnode.attrs.items, vnode.attrs.onDishRemoved) : m('.block', 'На эту дату нет меню')
    }
  }

  function sqlDateFormat (date) {
    return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2)
  }

  function getDateString (date) {
    const days = ['воскресенье', 'понедельник', 'вторник', 'среду', 'четверг', 'пятницу', 'субботу']
    const months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
    const dateObject = new Date(date)
    const dateString = days[dateObject.getDay()] + ', ' + dateObject.getDate() + ' ' + months[dateObject.getMonth()]
    return dateString
  }

  return {
    selectedDate: new Date(),
    menuItems: [],
    allDatesMenu: [],
    loadMenuItems () {
      MenuStore.fetchItem(sqlDateFormat(this.selectedDate))
        .then(items => {
          this.menuItems = (items ? items.sort((el1, el2) => el1.typeOrder > el2.typeOrder) : [])
          lastDishTypeId = null
        })
        .catch(err => console.error(err))
    },
    oninit () {
      this.loadMenuItems(this.selectedDate)
      MenuStore.fetchAllDate()
        .then(items => {
          this.allDatesMenu = (items ? items.map(el => el.menuDate) : [])
        })
        .catch(err => console.error(err))
    },
    setNewDate (newValue) {
      this.selectedDate = newValue
      MenuDate = sqlDateFormat(newValue)
      this.loadMenuItems()
      m.redraw()
    },
    view (vnode) {
      return [
        m('.columns',
          m('.column.is-half-tablet.is-one-third-widescreen',
            m('.field.is-horizontal',
              m('.field-label.is-wide.is-normal',
                m('label.label', 'Выбор даты:')
              ),
              m('.field-body',
                m('.field is-narrow',
                  this.allDatesMenu.length > 0 ? m(DateFieldComp, { dates: this.allDatesMenu, value: this.selectedDate, onUpdate: newValue => { this.setNewDate(newValue) } }) : 'Loading'
                )
              )
            ),
            m(AddDishPanelComp, { menuDate: MenuDate, alreadyAddedDishes: this.menuItems, onDishAdded: () => this.loadMenuItems() })
          ),
          m('.column.is-half-tablet',
            m('.block', m('.title.is-size-4-touch', 'Меню на ' + getDateString(this.selectedDate))),
            MenuStore.itemLoaded
              ? m(TableComp, { items: this.menuItems, onDishRemoved: () => this.loadMenuItems() })
              : m('progress.progress[max=100]')
          )
        ),
        m(DishFormComp)
      ]
    }
  }
}
