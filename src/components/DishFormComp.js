module.exports = ({ m, DishStore, DishTypeStore }) => {
  const SaveButton = {
    loading: false,
    submit () {
      this.loading = true
      DishStore.updateItem(ModalForm.item.id, ModalForm.item).then(() => { this.loading = false; ModalForm.hide() })
      DishStore.fetchList()
    },

    view () {
      return m('button.button.is-success',
        {
          class: this.loading ? 'is-loading' : '',
          onclick: () => SaveButton.submit()
        },
        'Сохранить'
      )
    }
  }

  const EditButton = {
    view () {
      return m('button.button.is-success',
        {
          class: ModalForm.confirmDelete ? 'is-hidden' : '',
          onclick: () => (ModalForm.readonly = false)
        },
        'Изменить'
      )
    }
  }

  const AddButton = {
    loading: false,
    submit () {
      this.loading = true
      DishStore.addItem(ModalForm.item).then(() => { this.loading = false; DishStore.fetchList(); ModalForm.hide() })
      DishStore.fetchList()
    },

    view () {
      return m('button.button.is-success',
        {
          class: (ModalForm.confirmDelete ? 'is-hidden ' : '') + (this.loading ? 'is-loading' : ''),
          onclick: () => { AddButton.submit(); ModalForm.readonly = true }
        },
        'Добавить'
      )
    }
  }

  const DeleteButtons = {
    loading: false,
    confirmDelete: false,
    submit () {
      this.loading = true
      DishStore.deleteItem(ModalForm.item.id).then(() => { this.loading = false; DishStore.fetchList(); ModalForm.hide() })
    },
    view () {
      return [
        m('button.button.level-item',
          {
            class: (DeleteButtons.confirmDelete) ? 'is-hidden' : '',
            onclick: () => (DeleteButtons.confirmDelete = true)
          },
          'Удалить'
        ),
        m('button.button.level-item',
          {
            class: DeleteButtons.confirmDelete ? '' : 'is-hidden',
            onclick: () => (DeleteButtons.confirmDelete = false)
          },
          'Не удалять'
        ),
        m('button.button.is-danger.level-item',
          {
            class: (DeleteButtons.confirmDelete ? '' : 'is-hidden ') + (this.loading ? 'is-loading' : ''),
            onclick: () => DeleteButtons.submit()
          },
          'Удалить окончательно!')
      ]
    }
  }

  const ModalForm = {
    class: '',
    readonly: true,
    isEmpty: () => ModalForm.item.id === undefined,
    item: {},

    hide () {
      ModalForm.class = ''
      DeleteButtons.confirmDelete = false
    },

    view () {
      return (
        m('.modal', { class: this.class },
          m('.modal-background', { onclick: () => ModalForm.hide() }),
          m('.modal-card',
            m('header.modal-card-head',
              m('p.modal-card-title', ''),
              m('button.delete[aria-label=close]', { onclick: () => ModalForm.hide() })
            ),
            m('section.modal-card-body',
              m('fieldset', { disabled: ModalForm.readonly },
                m('.field',
                  m('label.label', 'Название'),
                  m('.control',
                    m('input.input[type=text]', { oninput: e => { ModalForm.item.name = e.target.value }, value: ModalForm.item.name })
                  )
                ),
                m('.field',
                  m('label.label', 'Тип'),
                  m('.control',
                    m('.select',
                      m('select', {
                        oninput: (e) => {
                          const selectedType = DishTypeStore.map[e.target.value]
                          ModalForm.item.type = selectedType.type
                          ModalForm.item.typeId = selectedType.id
                          ModalForm.item.typeName = selectedType.name
                        }
                      },
                      m('option.has-text-weight-bold', { selected: !ModalForm.item.typeId }, 'Выберите тип:'),
                      DishTypeStore.list.map(type => m('option', { value: type.id, selected: type.id === ModalForm.item.typeId }, type.name))
                      )
                    )
                  )
                ),
                m('.field',
                  m('label.label', 'Описание'),
                  m('.control',
                    m('textarea.textarea', { oninput: e => { ModalForm.item.comment = e.target.value }, value: ModalForm.item.comment })
                  )
                )
              )
            )
            ,
            m('footer.modal-card-foot',
              m('.container.level.is-mobile',
                m('.level-left', ModalForm.isEmpty() || m(DeleteButtons)),
                m('.level-right',
                  DeleteButtons.confirmDelete || (
                    m('.level-item',
                      ModalForm.isEmpty() ? m(AddButton) : (ModalForm.readonly ? m(EditButton) : m(SaveButton))
                    )
                  )
                )
              )
            )
          )
        )
      )
    }
  }

  return {
    showEditForm (item) { ModalForm.item = item; ModalForm.readonly = true; ModalForm.class = 'is-active' },
    showAddForm () { ModalForm.item = { comment: 'описание готовится' }; ModalForm.readonly = false; ModalForm.class = 'is-active' },
    oninit (vnode) { DishTypeStore.fetchList() },
    view (vnode) {
      return m(ModalForm)
    }
  }
}
