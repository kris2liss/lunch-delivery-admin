module.exports = ({ m, ApiKeyStore }) => () => ({
  oninit () {
    ApiKeyStore.clear()
    m.route.set('/')
    document.location.reload()
  },
  view () {
    return m('progress.progress[max=100]')
  }
})
