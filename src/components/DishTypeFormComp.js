module.exports = ({ m, DishTypeStore }) => {
  const SaveButton = {
    loading: false,
    submit () {
      this.loading = true
      DishTypeStore.updateItem(ModalForm.item.id, ModalForm.item).then(() => { this.loading = false; ModalForm.hide() })
    },

    view () {
      return m('button.button.is-success',
        {
          class: this.loading ? 'is-loading' : '',
          onclick: () => SaveButton.submit()
        },
        'Сохранить'
      )
    }
  }

  const EditButton = {
    view () {
      return m('button.button.is-success',
        {
          class: ModalForm.confirmDelete ? 'is-hidden' : '',
          onclick: () => (ModalForm.readonly = false)
        },
        'Изменить'
      )
    }
  }

  const AddButton = {
    loading: false,
    submit () {
      this.loading = true
      DishTypeStore.addItem(ModalForm.item).then(() => { this.loading = false; DishTypeStore.fetchList(); ModalForm.hide() })
    },

    view () {
      return m('button.button.is-success',
        {
          class: (ModalForm.confirmDelete ? 'is-hidden ' : '') + (this.loading ? 'is-loading' : ''),
          onclick: () => { AddButton.submit(); ModalForm.readonly = true }
        },
        'Добавить'
      )
    }
  }

  const DeleteButtons = {
    loading: false,
    confirmDelete: false,
    submit () {
      this.loading = true
      DishTypeStore.deleteItem(ModalForm.item.id).then(() => { this.loading = false; DishTypeStore.fetchList(); ModalForm.hide() })
    },
    view () {
      return [
        m('button.button.level-item',
          {
            class: (DeleteButtons.confirmDelete) ? 'is-hidden' : '',
            onclick: () => (DeleteButtons.confirmDelete = true)
          },
          'Удалить'
        ),
        m('button.button.level-item',
          {
            class: DeleteButtons.confirmDelete ? '' : 'is-hidden',
            onclick: () => (DeleteButtons.confirmDelete = false)
          },
          'Не удалять'
        ),
        m('button.button.is-danger.level-item',
          {
            class: (DeleteButtons.confirmDelete ? '' : 'is-hidden') + (this.loading ? 'is-loading' : ''),
            onclick: () => DeleteButtons.submit()
          },
          'Удалить окончательно!')
      ]
    }
  }

  const ModalForm = {
    class: '',
    readonly: true,
    isEmpty: () => ModalForm.item.id === undefined,
    item: {},

    hide () {
      ModalForm.class = ''
      DeleteButtons.confirmDelete = false
    },

    view () {
      return (
        m('.modal', { class: this.class },
          m('.modal-background', { onclick: () => ModalForm.hide() }),
          m('.modal-card',
            m('header.modal-card-head',
              m('p.modal-card-title', ''),
              m('button.delete[aria-label=close]', { onclick: () => ModalForm.hide() })
            ),
            m('section.modal-card-body',
              m('fieldset', { disabled: ModalForm.readonly },
                m('.field',
                  m('label.label', 'Название'),
                  m('.control',
                    m('input.input[type=text]', { oninput: e => { ModalForm.item.name = e.target.value }, value: ModalForm.item.name })
                  )
                ),
                m('.field',
                  m('label.label', 'Тип'),
                  m('.control',
                    m('input.input[type=text]', { oninput: e => { ModalForm.item.type = e.target.value }, value: ModalForm.item.type })
                  )
                ),
                m('.field',
                  m('label.label', 'Порядок'),
                  m('.control',
                    m('input.input[type=text]', { oninput: e => { ModalForm.item.order = e.target.value }, value: ModalForm.item.order })
                  )
                ),
                m('.field',
                  m('label.label', 'Цена'),
                  m('.control',
                    m('input.input[type=text]', { oninput: e => { ModalForm.item.price = e.target.value }, value: ModalForm.item.price })
                  )
                )
              )
            )
            ,
            m('footer.modal-card-foot',
              m('.container.level.is-mobile',
                m('.level-left', ModalForm.isEmpty() || m(DeleteButtons)),
                m('.level-right',
                  DeleteButtons.confirmDelete || (
                    m('.level-item',
                      ModalForm.isEmpty() ? m(AddButton) : (ModalForm.readonly ? m(EditButton) : m(SaveButton))
                    )
                  )
                )
              )
            )
          )
        )
      )
    }
  }

  return {
    showEditForm (item) { ModalForm.item = item; ModalForm.readonly = true; ModalForm.class = 'is-active' },
    showAddForm () { ModalForm.item = {}; ModalForm.readonly = false; ModalForm.class = 'is-active' },
    oninit (vnode) { DishTypeStore.fetchList() },
    view (vnode) {
      return m(ModalForm)
    }
  }
}
