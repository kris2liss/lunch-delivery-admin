module.exports = ({ m, BulmaCalendarComp }) => () => ({
  oncreate (vnode) {
    const calendar = BulmaCalendarComp.attach(vnode.dom,
      {
        type: 'date',
        showHeader: false,
        dateFormat: 'dd.MM.yyyy',
        showFooter: false,
        weekStart: 1,
        displayMode: 'dialog',
        highlightedDates: vnode.attrs.dates
      }
    )[0]
    calendar.on('select', () => {
      vnode.attrs.onUpdate(calendar.startDate)
    })
    calendar.value(vnode.attrs.value)
  },
  view (vnode) {
    return m('span')
  }
})
