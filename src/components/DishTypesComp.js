module.exports = ({ m, DishTypeStore, DishTypeFormComp }) => () => {
  const RowMarkup = ({ item }) => (
    m('tr.is-clickable',
      { onclick: () => { DishTypeFormComp.showEditForm(item) } },
      m('td', item.id),
      m('td', item.name),
      m('td.is-hidden-mobile', item.type),
      m('td.is-hidden-mobile', item.order),
      m('td.is-hidden-mobile', item.price)
    )
  )

  const TableMarkup = (items) => (
    m('table.table.is-hoverable',
      m('tbody',
        m('tr',
          [
            m('th', 'ID'),
            m('th', 'Название'),
            m('th.is-hidden-mobile', 'Тип'),
            m('th.is-hidden-mobile', 'Порядок'),
            m('th.is-hidden-mobile', 'Цена')
          ]
        ),
        items.map(item => RowMarkup({ item }))
      )
    )
  )

  const TableComp = {
    view (vnode) {
      const items = vnode.attrs.items
      return TableMarkup(items)
    }
  }

  const AddButtonComp = {
    view: () => m('.block',
      m('button.button',
        {
          onclick () {
            DishTypeFormComp.showAddForm()
          }
        },
        'Добавить')
    )
  }

  return {
    oninit (vnode) {
      DishTypeStore.fetchList()
    },
    view (vnode) {
      return [
        m('.title', 'Типы блюд'),
        DishTypeStore.listLoaded
          ? [m(AddButtonComp), m(TableComp, { items: DishTypeStore.list }), m(DishTypeFormComp)]
          : m('progress.progress[max=100]')
      ]
    }
  }
}
