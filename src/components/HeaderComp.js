import logoImage from 'url:../resources/images/logo.png?as=webp&height=28'

module.exports = ({ m }) => {
  const navMarkup = () => (
    m('nav.navbar.is-primary',
      { role: 'navigation', 'aria-label': 'dropdown navigation' }, [
        m('.navbar-brand',
          m('a.navbar-item', { href: '', oncreate: m.route.link },
            m('img', { src: logoImage, alt: '' })
          ),
          m('.navbar-burger[data-target="navbar-menu"]', { role: 'button', 'aria-label': 'menu', 'aria-expanded': 'false' },
            m('span'),
            m('span'),
            m('span')
          )
        ),
        m('.navbar-menu[id="navbar-menu"]',
          m('.navbar-start',
            m('a.navbar-item', { href: '#!/entities/menus' }, 'Меню'),
            m('a.navbar-item', { href: '#!/entities/dishes' }, 'Блюда'),
            m('a.navbar-item', { href: '#!/entities/dish-types' }, 'Типы блюд'),
            m('a.navbar-item', { href: '#!/logout' }, 'Выйти')
          )
        )
      ])
  )

  return {
    oncreate: () => {
      const navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0)
      if (navbarBurgers.length > 0) {
        navbarBurgers.forEach(el => {
          el.addEventListener('click', () => {
            const dataTarget = el.dataset.target
            const target = document.getElementById(dataTarget)
            el.classList.toggle('is-active')
            target.classList.toggle('is-active')
          })
        })
        window.onhashchange = () => {
          navbarBurgers.forEach(el => {
            const dataTarget = el.dataset.target
            const target = document.getElementById(dataTarget)
            el.classList.remove('is-active')
            target.classList.remove('is-active')
          })
        }
      }
    },

    view: vnode => navMarkup()
  }
}
