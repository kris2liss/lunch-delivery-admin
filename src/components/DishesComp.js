module.exports = ({ m, DishStore, DishTypeStore, DishFormComp }) => () => {
  const RowMarkup = ({ item }) => (
    m('tr.is-clickable',
      { onclick: () => { DishFormComp.showEditForm(item) } },
      m('td', item.id),
      m('td', item.name),
      m('td.is-hidden-mobile', item.typeName),
      m('td.is-hidden-touch', item.comment)
    )
  )

  const TableMarkup = (items) => (
    m('table.table.is-hoverable',
      m('tbody',
        m('tr',
          [
            m('th', 'ID'),
            m('th', 'Название'),
            m('th.is-hidden-mobile', 'Тип'),
            m('th.is-hidden-touch', 'Описание')
          ]
        ),
        items.map(item => RowMarkup({ item }))
      )
    )
  )

  const TableComp = {
    view (vnode) {
      const items = vnode.attrs.items
      return TableMarkup(items)
    }
  }

  const AddButtonComp = {
    view: () => m('.block',
      m('button.button',
        {
          onclick () {
            DishFormComp.showAddForm()
          }
        },
        'Добавить')
    )
  }

  return {
    oninit (vnode) {
      DishStore.fetchList()
      if (!DishTypeStore.listLoaded) {
        DishTypeStore.fetchList()
      }
    },
    view (vnode) {
      return [
        m('.title', 'Блюда'),
        DishStore.listLoaded & DishTypeStore.listLoaded
          ? [m(AddButtonComp), m(TableComp, { items: DishStore.list }), m(DishFormComp)]
          : m('progress.progress[max=100]')
      ]
    }
  }
}
