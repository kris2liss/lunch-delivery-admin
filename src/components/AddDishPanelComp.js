module.exports = ({ m, BulmaCollapsibleComp, DishStore, MenuStore, DateFieldComp, DishFormComp }) => () => {
  let MenuDate
  const typeColor = ['', '', 'is-dark', 'is-success', 'is-primary', 'is-link', 'is-info', 'is-warning', 'is-light']

  const filterArray = (array, value, limit) => {
    return array.filter(
      dish => dish.name.toLowerCase().includes(
        value.toLowerCase())
    ).slice(0, limit)
  }

  function sqlDateFormat (date) {
    return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2)
  }

  const AddButtonComp = {
    view: () => m('.block',
      m('button.button',
        {
          onclick () {
            DishFormComp.showAddForm()
          }
        },
        'Создать новое блюдо')
    )
  }

  function sortDishes (dish1, dish2) {
    let result = 0
    if (dish1.typeOrder === dish2.typeOrder) {
      if (dish1.name < dish2.name) {
        result = -1
      } else {
        result = 1
      }
    } else {
      if (dish1.typeOrder < dish2.typeOrder) {
        result = -1
      } else {
        result = 1
      }
    }

    return result
  }

  const searchField = {
    value: '',
    onupdate () {
      if (value !== '') {
        PanelComp.filterItems(DishStore.list, searchField.value)
      }
    },
    view: () => m('input.input[type=text][placeholder=Поиск блюд]',
      {
        oninput: e => {
          searchField.value = e.target.value
          PanelComp.filterItems(DishStore.list, e.target.value)
        },
        value: searchField.value
      }
    )
  }

  const PanelComp = {
    items: [],
    loading: false,
    selectedItems: new Map(),
    filterItems (items, value) {
      PanelComp.items = value.length > 0 ? filterArray(items, value, 6) : []
    },
    addSelectedDishToMenu (callback) {
      this.loading = true
      Promise.all(
        Array.from(this.selectedItems.values())
          .map(dish => MenuStore.addDishToMenu(dish.id, MenuDate)))
        .then(() => {
          this.loading = false
          this.selectedItems.clear()
          callback()
        })
    },
    showDishFromMenu (menuDate) {
      MenuStore.fetchItem(sqlDateFormat(menuDate)).then(() => (searchField.value = '', PanelComp.items = MenuStore.list))
    },
    view (vnode) {
      return m('nav.panel',
        m('p.panel-heading.is-clickable[href=#AddDishPanel][data-action=collapse]',
          m('.is-size-6', 'Добавление блюд')
        ),
        m('#AddDishPanel.is-collapsible',
          m('.panel-block', 'Показать блюда из существущего меню'),
          m('.panel-block',
            m('p.control',
              m(DateFieldComp, { value: new Date(), onUpdate: newValue => { this.showDishFromMenu(newValue) } })
            )
          ),
          m('.panel-block', m(AddButtonComp)),
          m('.panel-block', 'Найдите и добавьте необходимы блюда'),
          m('.panel-block',
            m('p.control',
              m(searchField)
            )
          ),
          PanelComp.items.sort(sortDishes).map(item => (
            m('a.panel-block',
              {
                onclick: () => this.selectedItems.has(item.id) ? this.selectedItems.delete(item.id) : this.selectedItems.set(item.id, item)
              },
              m('input[type=checkbox]',
                {
                  disabled: vnode.attrs.alreadyAddedDishes.some(dish => dish.id === item.id),
                  checked: vnode.attrs.alreadyAddedDishes.some(dish => dish.id === item.id) || this.selectedItems.has(item.id),
                  title: vnode.attrs.alreadyAddedDishes.some(dish => dish.id === item.id) ? 'Уже добавлено' : ''
                }),
              m('.block', 
                m('span.has-text-weight-semibold', m('span.tag.mr-2', { class: typeColor[item.typeId]}, item.typeName), item.name),
                m('br'),
                m('span.is-size-7', item.comment)
              )
            ),
            
          )),
          m('.panel-block',
            m('p.control',
              m('button.button',
                {
                  class: this.loading ? 'is-loading' : '',
                  disabled: this.selectedItems.size === 0,
                  onclick: e => PanelComp.addSelectedDishToMenu(vnode.attrs.onDishAdded)
                },
                'Добавить')
            )
          )
        )
      )
    }
  }

  return {
    oncreate (vnode) {
      if (document.getElementById('main').offsetWidth < 750) {
        const el = vnode.dom.querySelector('#AddDishPanel')
        vnode.state.panel = new BulmaCollapsibleComp(el)
      }
    },
    oninit () {
      DishStore.fetchList()
    },
    onupdate (vnode) {
      MenuDate = vnode.attrs.menuDate
    },
    view (vnode) {
      return m(PanelComp, { alreadyAddedDishes: vnode.attrs.alreadyAddedDishes, onDishAdded: vnode.attrs.onDishAdded })
    }
  }
}
