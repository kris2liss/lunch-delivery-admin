module.exports = ({ m }) => {
  return {
    view: () => m('footer.footer',
      m('.content.has-text-centered',
        'Lubeer (c) 2021'
      )
    )
  }
}
