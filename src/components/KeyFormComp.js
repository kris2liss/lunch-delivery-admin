module.exports = ({ m, ApiKeyStore }) => {
  const SaveButton = {
    submit () {
      ApiKeyStore.saveKey(ModalForm.key)
      document.location.reload()
    },

    view () {
      return m('button.button.is-success',
        {
          disabled: ModalForm.key.length === 0,
          onclick: () => SaveButton.submit()
        },
        'Сохранить'
      )
    }
  }

  const ModalForm = {
    class: '',
    key: '',

    hide () {
      ModalForm.class = ''
    },

    view () {
      return (
        m('.section',
          m('.container',
            m('.modal.is-active',
              m('.modal-background'),
              m('.modal-card',
                m('header.modal-card-head',
                  m('p.modal-card-title.is-size-5', 'Введите код для доступа к данным')
                ),
                m('section.modal-card-body',
                  m('fieldset',
                    m('.field',
                      m('label.label', 'Код'),
                      m('.control',
                        m('input.input[type=text]', { oninput: e => { ModalForm.key = e.target.value } })
                      )
                    )
                  )
                ),
                m('footer.modal-card-foot',
                  m('.container.level.is-mobile',
                    m('.level-right',
                      m(SaveButton)
                    )
                  )
                )
              )
            )
          )
        )
      )
    }
  }

  return {
    view: vnode => m(ModalForm)
  }
}
