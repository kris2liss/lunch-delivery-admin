import ApiConfig from '../ApiConfig'

module.exports = ({ m, ApiStatus, ApiKeyStore }) => (method, url, body = null) => {
  return ApiStatus.status
    ? m.request({
        method: method,
        url: ApiConfig.url + url,
        withCredentials: true,
        headers: {
          Authorization: 'Basic ' + ApiKeyStore.key
        },
        body
      })
        .then(result => {
          if (result.statusCode === 200) {
            return result
          } else {
            ApiStatus.status = false
          }
        })
        .catch(err => {
          if(err.code !== 409) {
            ApiStatus.error = err
            ApiStatus.status = false
          }
        })
    : Promise.reject(ApiStatus.error)
}
