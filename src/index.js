import './styles.sass'
import c from './di'

const app = ({ m, Layout, MenusComp, DishesComp, DishTypesComp, ApiKeyStore, KeyFormComp, LogoutComp }) => {
  if (ApiKeyStore.loadKey()) {
    m.mount(document.body, Layout)

    const content = document.getElementById('content')

    m.route(content, '/entities/menus', {
      '/entities/menus': MenusComp,
      '/entities/dishes': DishesComp,
      '/entities/dish-types': DishTypesComp,
      '/logout': LogoutComp
    })
  } else {
    m.mount(document.body, KeyFormComp)
  }
}

app(c)
