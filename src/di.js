import m from 'mithril'

import bulmaCalendar from 'bulma-calendar'
import bulmaCollapsible from '@creativebulma/bulma-collapsible/src/js/index.js'

import HeaderComp from './components/HeaderComp'
import FooterComp from './components/FooterComp'
import DishFormComp from './components/DishFormComp'
import DishesComp from './components/DishesComp'
import MenusComp from './components/MenusComp'
import AddDishPanelComp from './components/AddDishPanelComp'
import DateFieldComp from './components/DateFieldComp'
import DishTypeFormComp from './components/DishTypeFormComp'
import KeyFormComp from './components/KeyFormComp'
import DishTypesComp from './components/DishTypesComp'
import LogoutComp from './components/LogoutComp'
import DishStore from './stores/DishStore'
import MenuStore from './stores/MenuStore'
import DishTypeStore from './stores/DishTypeStore'
import ApiStatus from './stores/ApiStatus'
import ApiKeyStore from './stores/ApiKeyStore'
import ApiClient from './ApiClient'
import Layout from './Layout'

const c = {
  m,
  HeaderComp,
  FooterComp,
  DishFormComp,
  DishesComp,
  MenusComp,
  DishStore,
  MenuStore,
  DishTypeStore,
  DateFieldComp,
  DishTypeFormComp,
  DishTypesComp,
  AddDishPanelComp,
  LogoutComp,
  ApiClient,
  ApiKeyStore,
  Layout,
  ApiStatus,
  KeyFormComp,
  BulmaCalendarComp: bulmaCalendar,
  BulmaCollapsibleComp: bulmaCollapsible
}

c.ApiStatus = ApiStatus(c)
c.ApiKeyStore = ApiKeyStore(c)
c.ApiClient = ApiClient(c)
c.KeyFormComp = KeyFormComp(c)

c.DishStore = DishStore(c)
c.MenuStore = MenuStore(c)
c.DishTypeStore = DishTypeStore(c)

c.DishFormComp = DishFormComp(c)
c.DishesComp = DishesComp(c)
c.LogoutComp = LogoutComp(c)

c.DateFieldComp = DateFieldComp(c)
c.AddDishPanelComp = AddDishPanelComp(c)
c.MenusComp = MenusComp(c)
c.DishTypeFormComp = DishTypeFormComp(c)
c.DishTypesComp = DishTypesComp(c)

c.HeaderComp = HeaderComp(c)
c.FooterComp = FooterComp(c)

c.Layout = Layout(c)

module.exports = c
