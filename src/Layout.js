module.exports = ({ m, HeaderComp, FooterComp, DishesComp, MenusComp, DishTypesComp, ApiStatus }) => () => {
  const ServerUnresponsebleModal = {
    view () {
      return m('.modal', { class: ApiStatus.status ? '' : 'is-active' },
        m('.modal-background'),
        m('.modal-content',
          m('.box', 'Сервер с данными не доступен. Проверьте соединение и обновите страницу.')
        )
      )
    }
  }

  return {
    view: vnode => m('.main#main',
      m(HeaderComp),
      m('.section',
        m('.container#content')
      ),
      m(FooterComp),
      m(ServerUnresponsebleModal)
    )
  }
}
