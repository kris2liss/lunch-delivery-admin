module.exports = ({ m, ApiClient }) => ({
  itemLoaded: false,
  listLoaded: false,

  list: [],
  map: {},
  item: null,

  fetchList () {
    this.listLoaded = false
    return ApiClient('GET', '/dish-types')
      .then(result => {
        if (result.data.length > 0) {
          this.list = result.data
          this.map = Object.fromEntries(result.data.map(el => [el.id, el]))
        }
        this.listLoaded = true
      })
  },

  updateItem (id, { name, type, order, price }) {
    this.itemUpdated = false
    return ApiClient('PATCH', '/dish-types/' + id, { name, type, order, price })
      .then(result => {
        if (result.statusCode === 200) {
          this.itemUpdated = true
        }
      })
  },

  addItem ({ name, type, order, price }) {
    this.itemAdded = false
    return ApiClient('POST', '/dish-types', { name, type, order, price })
      .then(result => {
        if (result.statusCode === 200) {
          this.itemAdded = true
        }
      })
  },

  deleteItem (id) {
    this.itemLoaded = false
    return ApiClient('DELETE', '/dish-types/' + id)
      .then(result => {
        if (result.statusCode === 200) {
          this.itemDeleted = true
        }
      })
  }
})
