module.exports = ({ m, ApiClient }) => ({
  itemLoaded: false,
  datesLoaded: false,
  item: [],
  list: [],
  datesList: [],

  dishAddedToMenu: false,
  dishRemovedFromMenu: false,

  fetchItem (id) {
    this.itemLoaded = false
    return ApiClient('GET', '/menus/' + id)
      .then(result => {
        if (result.data.length > 0) {
          this.list = result.data
        } else {
          this.list = []
        }
        this.itemLoaded = true
        return this.list
      })
  },

  fetchAllDate () {
    this.datesLoaded = false
    return ApiClient('GET', '/menus/all-dates')
      .then(result => {
        if (result.data.length > 0) {
          this.datesList = result.data
        } else {
          this.datesList = []
        }
        this.datesLoaded = true
        return this.datesList
      })
  },

  addDishToMenu (dishId, menuDate) {
    this.dishAddedToMenu = false
    return ApiClient('POST', '/menus/' + menuDate + '/dishes/' + dishId)
      .then(result => {
        if (result.statusCode === 200) {
          this.dishAddedToMenu = true
        }
      })
  },

  removeDishFromMenu (dishId, menuDate) {
    this.dishRemovedFromMenu = false
    return ApiClient('DELETE', '/menus/' + menuDate + '/dishes/' + dishId)
      .then(result => {
        if (result.statusCode === 200) {
          this.dishRemovedFromMenu = true
        }
      })
  }
})
