module.exports = ({ m }) => ({
  key: null,
  loadKey () {
    const key = window.localStorage.getItem('api_key_write')
    if (key) {
      this.key = key
      return true
    } else {
      return false
    }
  },
  saveKey (key) {
    window.localStorage.setItem('api_key_write', window.btoa('api_key_write' + ':' + key))
  },
  clear () {
    window.localStorage.removeItem('api_key_write')
  }
})
