module.exports = ({ m, ApiClient }) => ({
  itemLoaded: false,
  listLoaded: false,

  list: [],
  item: null,

  fetchList () {
    this.listLoaded = false
    return ApiClient('GET', '/dishes')
      .then(result => {
        if (result.data.length > 0) {
          this.list = result.data
        }
        this.listLoaded = true
      })
  },

  fetchItem (id) {
    this.itemLoaded = false
    return ApiClient('GET', '/dishes/' + id)
      .then(result => {
        if (result.data.length > 0) {
          this.list = result.data[0]
        }
        this.itemLoaded = true
      })
  },

  updateItem (id, { name, comment, typeId }) {
    this.itemUpdated = false
    return ApiClient('PATCH', '/dishes/' + id, { name, comment, typeId })
      .then(result => {
        if (result.statusCode === 200) {
          this.itemUpdated = true
        }
      })
  },

  addItem ({ name, comment, typeId }) {
    this.itemAdded = false
    return ApiClient('POST', '/dishes', { name, comment, typeId })
      .then(result => {
        if (result.statusCode === 200) {
          this.itemAdded = true
        }
      })
  },

  deleteItem (id) {
    this.itemLoaded = false
    return ApiClient('DELETE', '/dishes/' + id)
      .then(result => {
        if (result.statusCode === 200) {
          this.itemDeleted = true
        }
      })
  }

})
